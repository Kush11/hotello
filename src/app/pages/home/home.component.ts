import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Hotel } from 'src/app/models/hotel';
import { Room } from 'src/app/models/room';
import { AppDataService } from 'src/app/services/data/app/app.data.service';
import * as RoomActions from 'src/app/state/actions/rooms.actions';
import AppState from 'src/app/state/app.state';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {
  public hotel$: Observable<AppState>;
  public hotelSubscription: Subscription;
  public hotelList: Hotel[];
  public hotelError: Error = null;
  public rooms = [];
  public selectedRoomObject: Room;
  public state: { hotel: AppState; };
  selectedRoomId: Number;
  selectedRoom: any;

  constructor(private store: Store<{ hotel: AppState }>, private route: Router) {
    this.hotel$ = store.pipe(select('hotel'));
  }

  public ngOnInit() {
    this.hotelSubscription = this.hotel$
      .pipe(
        map((x) => {
          this.hotelList = x.hotel;
          this.hotelError = x.hotelError;
          this.hotelList.forEach((r) => {
          });
        }),
      ).subscribe();
    this.selectRoom(1);
  }

  public selectRoom(value) {
    console.log(value)
    this.store.select((x) => this.state = x).subscribe();
    this.state.hotel.hotel.forEach((element) => {
      this.rooms = [...this.rooms, element.rooms];
    });
    this.selectedRoomId = Number(value);
    const newRoom = this.rooms[0].find((x) => x.id === this.selectedRoomId);
    this.selectedRoom = newRoom;
    console.log(this.selectedRoom, this.selectedRoomId);
  }

  public checkAvailability() {
    if (this.selectedRoom) {
      if (this.selectedRoom.isBooked) {
        alert(`${this.selectedRoom.name}, has been booked please check another room`);
      } else {
        this.route.navigate(['/room', this.selectedRoom.id]);
      }
    } else {
      return;
    }
  }

  public ngOnDestroy() {
    if (this.hotelSubscription) {
      this.hotelSubscription.unsubscribe();
    }
  }
}
