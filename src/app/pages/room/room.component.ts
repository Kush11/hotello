import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import AppState from 'src/app/state/app.state';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Subscription, Observable } from 'rxjs';
import { Room } from 'src/app/models/room';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {
  rooms$: Observable<AppState>;
  roomSubscription: Subscription;
  roomList: Room[];
  roomError: any;

  constructor(private store: Store<{ hotel: AppState }>, private route: Router) {
    this.rooms$ = this.store.pipe(select('hotel'));
  }

  ngOnInit() {

    this.roomSubscription = this.rooms$
      .pipe(
        map((x) => {
          this.roomList = x.rooms;
          this.roomError = x.hotelError;
        }),
      ).subscribe();

  }


  bookRoom() {
  }

}
