import { Occupant } from './occupant';

export class Room {
  public id: string;
  public name: string;
  public number: string;
  public isBooked: boolean;
  public bookTime: string;
  public price: string;
  public roomType: RoomType;
  public occupant: Occupant[];
}

export class RoomType {
  public id: string;
  public title: string;
  public roomImageUrl: string;
  public bedSize: string;
  public allowedOccupants: string;
}
