export class Occupant {
  public id: string;
  public firstName: string;
  public lastName: string;
  public phoneNumber: string;
  public address: string;
}

export class CheckIn {
  public id: string;
  public checkInTime: string;
  public checkOutTime: string;
}
