import { Room } from './room';

export class Hotel {
  public id: string;
  public name: string;
  public address: string;
  public phoneNumber: string;
  public email: string;
  public images: [];
  public rooms: Room[];
}
