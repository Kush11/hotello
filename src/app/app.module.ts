import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { HomeComponent } from './pages/home/home.component';
import { AppDataService } from './services/data/app/app.data.service';
import { AppWebService } from './services/data/app/app.web.service';
import { FooterComponent } from './shared/footer/footer.component';
import { HeaderComponent } from './shared/header/header.component';
import { HotelEffects } from './state/effects/hotel.effects';
import { HotelReducer } from './state/reducers/hotel.reducer';
import { RoomComponent } from './pages/room/room.component';
import { RoomEffects } from './state/effects/room.effects';
import { RoomReducer } from './state/reducers/room.reducer';

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    RoomComponent,
  ],
  entryComponents: [],

  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreDevtoolsModule.instrument({
      maxAge: 10,
    }),
    StoreModule.forRoot({ hotel: HotelReducer }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([HotelEffects, RoomEffects]),
  ],
  providers: [
    { provide: AppDataService, useClass: AppWebService },
  ],
})
export class AppModule { }
