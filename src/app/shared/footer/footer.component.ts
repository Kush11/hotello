import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { Subscription } from 'rxjs/internal/Subscription';
import { Hotel } from 'src/app/models/hotel';
import { Room } from 'src/app/models/room';
import AppState from 'src/app/state/app.state';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  public hotel$: Observable<AppState>;
  public hotelSubscription: Subscription;
  public hotelList: Hotel[];
  public hotelError: Error = null;
  public rooms: Room[] = [];
  constructor(private store: Store<{ hotel: AppState }>) {
    this.hotel$ = this.store.pipe(select('hotel'));
  }

  public ngOnInit() {

    this.hotelSubscription = this.hotel$
      .pipe(
        map((x) => {
          this.hotelList = x.hotel;
          this.hotelError = x.hotelError;
          this.hotelList.forEach((r) => {
            this.rooms = r.rooms;
          });
        }),
      ).subscribe();
  }

  public ngOnDestroy() {
    if (this.hotelSubscription) {
      this.hotelSubscription.unsubscribe();
    }
  }

}
