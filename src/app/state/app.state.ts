import { Hotel } from '../models/hotel';
import { Room } from '../models/room';

export default class AppState {
  public hotel: Hotel[];
  public selectedroom: any;
  public hotelError: Error;
  public rooms: Room[];
}

export const initializeState = (): AppState => {
  return {
    hotel: [],
    hotelError: null,
    rooms: [],
    selectedroom: 0,
  };
};
