import { createAction, props } from '@ngrx/store';
import { Room } from 'src/app/models/room';


export const GetRoom = createAction('[Room] - Get Room');

export const SuccessGetRoom = createAction('[Room] - Success Get Room',
  props<{ payload: Room[] }>(),
);

export const ErrorRoom = createAction('[Room] - Error',
  props<Error>());
