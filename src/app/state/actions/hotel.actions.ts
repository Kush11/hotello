import { createAction, props } from '@ngrx/store';
import { Hotel } from 'src/app/models/hotel';

export const GetHotel = createAction('[Hotel] - Get Hotel');

export const SuccessGetHotel = createAction('[Hotel] - Success Get Hotel',
  props<{ payload: Hotel[] }>(),
);

export const ErrorHotel = createAction('[Hotel] - Error',
  props<Error>());
