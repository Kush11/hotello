import * as fromRooms from './rooms.actions';

describe('loadRoomss', () => {
  it('should return an action', () => {
    expect(fromRooms.loadRoomss().type).toBe('[Rooms] Load Roomss');
  });
});
