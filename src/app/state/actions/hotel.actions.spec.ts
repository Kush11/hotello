import * as fromHotel from './hotel.actions';

describe('loadHotels', () => {
  it('should return an action', () => {
    expect(fromHotel.GetHotel().type).toBe('[Hotel] Load Hotels');
  });
});
