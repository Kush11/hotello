import { createReducer, on, Action } from '@ngrx/store';
import * as RoomAction from '../actions/rooms.actions';
import RoomState, { initializeState } from '../app.state';
import AppState from '../app.state';

export const initialState = initializeState();

const reducer = createReducer(
  initialState,
  on(RoomAction.GetRoom, (state) => state),
  on(RoomAction.SuccessGetRoom, (state: AppState, { payload }) => {
    return { ...state, rooms: payload };
  }),
  // on(HotelAction.ErrorHotel, (state: HotelState, error: Error) => {
  //   return { ...state, hotelError: error };
  // }),
);

export function RoomReducer(state: RoomState | undefined, action: Action) {
  return reducer(state, action);
}
