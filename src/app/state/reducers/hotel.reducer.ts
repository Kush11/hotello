import { Action, createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import * as HotelAction from '../actions/hotel.actions';
import * as RoomAction from '../actions/rooms.actions';

import HotelState, { initializeState } from '../app.state';
import AppState from '../app.state';

export const initialState = initializeState();

const reducer = createReducer(
  initialState,
  on(HotelAction.GetHotel, (state) => state),
  on(HotelAction.SuccessGetHotel, (state: AppState, { payload }) => {
    return { ...state, hotel: payload };
  }),
  on(HotelAction.ErrorHotel, (state: HotelState, error: Error) => {
    return { ...state, hotelError: error };
  }),

  on(RoomAction.GetRoom, (state) => state),
  on(RoomAction.SuccessGetRoom, (state: AppState, { payload }) => {
    return { ...state, rooms: payload };
  }),
);

export function HotelReducer(state: HotelState | undefined, action: Action) {
  return reducer(state, action);
}
