import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { AppDataService } from 'src/app/services/data/app/app.data.service';
import * as RoomActions from '../actions/rooms.actions';
import { Room } from 'src/app/models/room';



@Injectable()
export class RoomEffects {
  constructor(private hotelService: AppDataService, private action$: Actions) { }

  public GetRooms$: Observable<Action> = createEffect(() =>
    this.action$.pipe(
      ofType(RoomActions.GetRoom),
      mergeMap((action) =>
        this.hotelService.getRooms()
          .pipe(map((data: Room[]) => {
            return RoomActions.SuccessGetRoom({ payload: data });
          }),
            catchError((error: Error) => {
              return of(RoomActions.ErrorRoom(error));
            }),
          )),
    ));
}
