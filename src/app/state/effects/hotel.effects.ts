import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { Hotel } from 'src/app/models/hotel';
import { AppDataService } from 'src/app/services/data/app/app.data.service';
import * as HotelActions from '../actions/hotel.actions';
import * as RoomActions from '../actions/rooms.actions';
import { Room } from 'src/app/models/room';



@Injectable()
export class HotelEffects {
  constructor(private hotelService: AppDataService, private action$: Actions) { }

  public GetHotel$: Observable<Action> = createEffect(() =>
    this.action$.pipe(
      ofType(HotelActions.GetHotel),
      mergeMap((action) =>
        this.hotelService.getAllHotels()
          .pipe(map((data: Hotel[]) => {
            return HotelActions.SuccessGetHotel({ payload: data });
          }),
            catchError((error: Error) => {
              return of(HotelActions.ErrorHotel(error));
            }),
          )),
    ));



}
