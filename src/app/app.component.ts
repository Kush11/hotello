import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as HotelActions from './state/actions/hotel.actions';
import * as RoomActions from './state/actions/rooms.actions';

import AppState from './state/app.state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  public title = 'hotello';

  constructor(private store: Store<AppState>) {
    this.store.dispatch(HotelActions.GetHotel());
    this.store.dispatch(RoomActions.GetRoom());
  }

}
