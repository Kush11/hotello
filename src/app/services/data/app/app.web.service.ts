import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Hotel } from 'src/app/models/hotel';
import { Room } from 'src/app/models/room';
import { environment } from 'src/environments/environment';
import { AppDataService } from './app.data.service';

@Injectable({ providedIn: 'root' })

export class AppWebService implements AppDataService {

  public baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = environment.mockBaseUrl;
  }

  public getAllHotels() {
    return this.http.get<Hotel[]>(`${this.baseUrl}/hotels`);
  }
  public getHotel(id) {
    return this.http.get<Hotel>(`${this.baseUrl}/hotels/${id}`);
  }
  public getRooms() {
    console.log('from service');
    return this.http.get<Room[]>(`${this.baseUrl}/rooms`);
  }
  public getRoom(id) {
    return this.http.get<Room>(`${this.baseUrl}/rooms/${id}`);
  }
}
