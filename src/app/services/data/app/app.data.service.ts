import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Hotel } from 'src/app/models/hotel';
import { Room } from 'src/app/models/room';

@Injectable()

export abstract class AppDataService {
  public abstract getAllHotels(): Observable<Hotel[]>;
  public abstract getHotel(id): Observable<Hotel>;
  public abstract getRooms(): Observable<Room[]>;
  public abstract getRoom(id): Observable<Room>;
}
